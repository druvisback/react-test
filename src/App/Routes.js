import React, {Component} from 'react';

import HomePage from "./pages/Home";
import NotFound from "./pages/NotFound";

import {Switch, BrowserRouter, HashRouter, Redirect} from 'react-router-dom';
import history from './modules/history';
import DynamicRouting from './components/DynamicRouting';

export const isHashRouter = true;
export const Router = isHashRouter?HashRouter:BrowserRouter;

class Routes extends Component{
    render(){
        return (
            <Router history={history}>
                <Switch>
                    <DynamicRouting
                        path={'/'}
                        component={<HomePage/>}
                    />
                    <DynamicRouting
                        path={'/404'}
                        component={<NotFound/>}
                    />
                    <Redirect from='*' to='/404' />
                </Switch>
            </Router>
        );
    }
}

export default Routes;
