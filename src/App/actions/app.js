import { createActions } from 'redux-actions';
import {ActionTypes_App} from '../constants';

export { goBack, go, push, replace } from '../modules/history';

export const {
    appLoaded: AppLoaded,

    appAlertHide: AlertHide,
    appAlertShow: AlertShow,

    appProgressTaskAdd: setNewProgress,
    appProgressTaskDone: completePreviousProgress,
    appProgressClear: clearAllProgress,

    appSaveText: SaveText,
    appDeleteText: DeleteText,
    appUpdateText: UpdateText,
} = createActions({
    //App loaded...
    [ActionTypes_App.LOADED]: () => ({}),

    [ActionTypes_App.SAVE_TEXT]: (txt) => ({txt}),
    [ActionTypes_App.DELETE_TEXT]: (index) => ({index}),
    [ActionTypes_App.UPDATE_TEXT]: (index, txt) => ({index, txt}),
});
