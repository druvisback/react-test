import React, {Fragment} from 'react';
import Middlewares from './Middlewares';
import {Route} from "react-router-dom";
import * as PropTypes from "prop-types";
import {isObject} from '../helper';

const DynamicRouting = (props)=>{
    let path = props.path;
    let myRoutes = null;
    if(isObject(path)){
        myRoutes = path;
    }else{
        myRoutes = [path];
    }
    if(myRoutes !== null) {
        return myRoutes.map((path, index) => {
            return (
                <Route
                    key={index}
                    path={props.prefix+path}
                    exact={props.exact}
                    component={() => (
                        <Fragment>
                            <Middlewares>
                                {props.isComponentVariable ? <props.component/> : props.component}
                            </Middlewares>
                        </Fragment>
                    )}
                />
            );
        });
    }else{
        return false;
    }
};

DynamicRouting.propsType = {
    path: PropTypes.oneOf([
        PropTypes.string,
        PropTypes.arrayOf(PropTypes.string),
    ]).isRequired,
    prefix: PropTypes.string.isRequired,
    exact: PropTypes.bool.isRequired,
    component: PropTypes.oneOf([
        PropTypes.element,
        PropTypes.object,
        PropTypes.any
    ]).isRequired,
    isComponentVariable: PropTypes.bool.isRequired
};
DynamicRouting.defaultProps = {
    path: false,
    exact: true,
    prefix: '',
    isComponentVariable: false
};

export default DynamicRouting;
