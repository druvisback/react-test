import React, {Component, useState, Fragment} from "react";
import Loader from 'react-loader-spinner';
import styled from "styled-components";
import {connect} from 'react-redux';

const LoaderWrapper = styled.div`
    display: flex;
    flex: 1;
    justify-content: center;
    align-items: center;
    position: absolute;
    width: 100%;
    height: 100%;
    z-index: 9;
    background-color: #000715b3;
`;

class MyLoader extends Component{
    state = {
        isLoaded: false
    };

    timeout = null;

    componentDidMount() {
        this.timeout = setTimeout(()=>{
            if(MyLoader && typeof MyLoader !== "undefined"){
                this.setState({isLoaded: true});
            }
        }, 700);
    }

    componentWillUnmount() {
        clearTimeout(this.timeout);
        this.setState({isLoaded: true});
    }

    render() {
        if(!this.state.isLoaded && !this.props.app_loaded) {
            return (
                <LoaderWrapper>
                    <Loader
                        type="BallTriangle"
                        color="#00BFFF"
                        height={100}
                        width={100}
                    />
                </LoaderWrapper>
            );
        }else{
            return (
                <Fragment>
                {this.props.children}
                </Fragment>
            );
        }
    }
}

const mapStoreToProps = (store) =>{
    return {
        app_loaded: store.app.loaded
    };
};

export default connect(mapStoreToProps, null)(MyLoader);
