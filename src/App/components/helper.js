import {store} from '../store';
import React from "react";

//Basic Functions...
export const formatDate = (strDate) => {
    let date = new Date(strDate);   
    const options = { year: "numeric", month: "short", day: "numeric", hour: "2-digit", minute: "2-digit" };
    return date.toLocaleDateString("en-US", options);
};
export const timeSince = (strDate, currentDate = false, debug = false) => {
  let date = new Date(strDate);
  if(!currentDate){
    currentDate = new Date();
  }
  const seconds = Math.floor((currentDate - date) / 1000);

  if(debug){
    console.log("----");
    console.log({hours: seconds / 3600});
    console.log({minuites: seconds / 60});
    console.log({seconds});
  }

  let interval = Math.floor(seconds / 31536000);

  if (interval > 1) {
    return interval + " years ago";
  }
  interval = Math.floor(seconds / 2592000);
  if (interval > 1) {
    return interval + " months ago";
  }
  interval = Math.floor(seconds / 86400);
  if (interval > 1) {
    return interval + " days ago";
  }



  interval = Math.floor(seconds / 3600);
  if (interval > 1) {
    return interval + " hours ago";
  }else{
    if(debug){
        console.log({hours: seconds / 3600});
      }
  }
  interval = Math.floor(seconds / 60);
  if (interval > 1) {
    return interval + " minutes ago";
  }
  if(seconds < 15){
    return "Just Now";
  }
  return Math.floor(seconds) + " seconds ago";
};
export const getMeta = (metaName) => {
    const metas = document.getElementsByTagName('meta');

    for (let i = 0; i < metas.length; i++) {
        if (metas[i].getAttribute('name') === metaName) {
            return metas[i].getAttribute('content');
        }
    }

    return false;
};
export const isObject = (object) => {;
    return (typeof object === 'object');
};
export const isDate = (date) => {
    return Object.prototype.toString.call(date) === '[object Date]';
}

export const validateEmail = (email) => {
    let re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
};
export const validatePassword = (password, minChar = false) => {
    if(password){
        return !(minChar && password.length > minChar);
    }
    return false;
};
export const getStore = ()=> {
    return store.getState();
};
export const getRootUrl = () => {
    let meta = getMeta('site');
    if(meta){
        return meta;
    }
    return window.location.origin
            ? window.location.origin + '/'
            : window.location.protocol + '/' + window.location.host + '/';
};
export const urlEncodedJSON = (srcjson) => {
    if(typeof srcjson !== "object")
        if(typeof console !== "undefined"){
            console.log("\"srcjson\" is not a JSON object");
            return null;
        }
    let u = encodeURIComponent;
    let urljson = "";
    let keys = Object.keys(srcjson);
    for(var i=0; i <keys.length; i++){
        urljson += u(keys[i]) + "=" + u(srcjson[keys[i]]);
        if(i < (keys.length-1))urljson+="&";
    }
    return urljson;
}
export const urlDecodedJSON = (urljson) => {
    let dstjson = {};
    let ret;
    let reg = /(?:^|&)(\w+)=(\w+)/g;
    while((ret = reg.exec(urljson)) !== null){
        dstjson[ret[1]] = ret[2];
    }
    return dstjson;
}
export const regexMatch = (str, regex) => {
    let m;
    let matches = [];
    while ((m = regex.exec(str)) !== null) {
        // This is necessary to avoid infinite loops with zero-width matches
        if (m.index === regex.lastIndex) {
            regex.lastIndex++;
        }
        
        // The result can be accessed through the `m`-variable.
        m.forEach((match, groupIndex) => {
            matches.push(match);
        });
    }
    return matches;
};
export const firstCaseUpper = (str = '')=>{
    if(!str){
        str = '';
    }
    if(str.length) {
        return str.charAt(0).toUpperCase() + str.slice(1);
    }
    return str;
};
export const getWords = (str = '') => {
    if(!str){
        str = '';
    }
    let result = str.split(" ");
    result = result.filter((item, index)=>{
        return item.length > 0;
    });
    return result;
};