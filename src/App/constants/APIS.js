//export const AUTH_SERVER_URL = 'https://50.116.38.194';
export const AUTH_SERVER_URL = 'http://127.0.0.1:8000';
export const SOCIAL_SERVER_URL = 'http://127.0.0.1:8001';

export const AUTH_API_URL = AUTH_SERVER_URL + '/api';
export const SOCIAL_API_URL = SOCIAL_SERVER_URL + '/api';

export const AUTH_APIS = {
    LOGIN: AUTH_API_URL+'/auth/login',
    BASIC_REGISTER: AUTH_API_URL+'/auth/basic-register',
    SOCIAL_VERIFY: AUTH_API_URL+'/auth/social-verification',
    SOCIAL_VERIFY_MAIL_CODE: AUTH_API_URL+'/auth/social-verification-mail-code',
    SOCIAL_REGISTER: AUTH_API_URL+'/auth/social-register',
    LOGOUT: AUTH_API_URL+'/auth/logout',
    REFRESH: AUTH_API_URL+'/auth/refresh',
    ME: AUTH_API_URL+'/auth/me',
    UPDATE: AUTH_API_URL+'/auth/update-profile',
    UPDATE_PASSWORD: AUTH_API_URL+'/auth/update-password',
    ACCOUNT_RECOVER: AUTH_API_URL+'/auth/account-recover',
    RESET_PASSWORD: AUTH_API_URL+'/auth/reset-password',
    UPDATE_FIRST_TIME_LOGIN_QUEST: AUTH_API_URL+'/auth/update-first-time-login-quest',
    GET_SERVER_INFO: AUTH_API_URL+'/server/get-server-info',
};

export const SOCIAL_APIS = {
    //SHARE POST -------------
    POST_SHARE: SOCIAL_API_URL+'/wall-section/post/share', 

    //POST -------------
    POST_GET: SOCIAL_API_URL+'/wall-section/post/get', 
    POST_CREATE: SOCIAL_API_URL+'/wall-section/post/create', 

    //Comment ----------
    COMMENTS_GET: SOCIAL_API_URL+'/wall-section/comment/get',
    COMMENT_CREATE: SOCIAL_API_URL+'/wall-section/comment/create',

    //Reply ----------
    REPLIES_GET: SOCIAL_API_URL+'/wall-section/reply/get',
    REPLY_CREATE: SOCIAL_API_URL+'/wall-section/reply/create',

    //Reaction ---------
        //Post
        REACTION_CREATE_ON_POST: SOCIAL_API_URL+'/wall-section/reaction/post/create',
        REACTION_REVOKE_ON_POST: SOCIAL_API_URL+'/wall-section/reaction/post/revoke',
        //Comment
        REACTION_CREATE_ON_COMMENT: SOCIAL_API_URL+'/wall-section/reaction/comment/create',
        REACTION_REVOKE_ON_COMMENT: SOCIAL_API_URL+'/wall-section/reaction/comment/revoke',

    //GALLERY -----------
    GALLERY_GET: SOCIAL_API_URL+'/gallery/get', 
    GALLERY_UPLOAD: SOCIAL_API_URL+'/gallery/upload',
    GALLERY_DELETE_FILE: SOCIAL_API_URL+'/gallery/delete-file',

    //SEARCH........
    SEARCH_USERS: SOCIAL_API_URL+'/wall-section/search/users',
    FETCH_PROFILE_DATA: SOCIAL_API_URL+'/wall-section/user/get',

    //CONNECTION......
    CONNECTION_REQUEST_MAKE: SOCIAL_API_URL+'/connection/make-request',
    CONNECTION_REQUEST_CANCEL: SOCIAL_API_URL+'/connection/cancel-request',
};

//ADMIN - Welcome Message
export const ADMIN_RESOURCES__WELCOME_MESSAGE_APIS = {
    GET: AUTH_API_URL+'/admin/resources/welcome-message/get-by-user-category', //POST, TOKEN, cat_uid,
    UPDATE: AUTH_API_URL+'/admin/resources/welcome-message/update-by-user-category', //POST, TOKEN, cat_uid, msg
};

//ADMIN - Denomination
export const ADMIN_RESOURCES__DENOMINATION_APIS = {
    GET_ALL: AUTH_API_URL+'/admin/resources/denomination/get-all',
    //GET: AUTH_API_URL+'/admin/resources/denomination/get',
    CREATE: AUTH_API_URL+'/admin/resources/denomination/create',
    //UPDATE: AUTH_API_URL+'/admin/resources/denomination/update',
    DELETE: AUTH_API_URL+'/admin/resources/denomination/delete',
};

//ADMIN - Denomination
export const ADMIN_RESOURCES__CATEGORY_GROUP_APIS = {
    CREATE: AUTH_API_URL+'/admin/resources/user-categories/groups/create',
    GET_ALL: AUTH_API_URL+'/admin/resources/user-categories/groups/all',
    DELETE: AUTH_API_URL+'/admin/resources/user-categories/groups/delete',
};

//ADMIN - Assign Walls
export const ADMIN_RESOURCES__ASSIGN_WALLS_APIS = {
    GET: AUTH_API_URL+'/admin/resources/assign-walls/get-by-user-category', //POST, TOKEN, cat_uid,
    UPDATE: AUTH_API_URL+'/admin/resources/assign-walls/update-by-user-category', //POST, TOKEN, cat_uid, msg
};

//ADMIN - Assign Roles
export const ADMIN_RESOURCES__ASSIGN_ROLES_APIS = {
    GET: AUTH_API_URL+'/admin/resources/assign-roles/get-by-user-category', //POST, TOKEN, cat_uid,
    UPDATE: AUTH_API_URL+'/admin/resources/assign-roles/update-by-user-category', //POST, TOKEN, cat_uid, msg
};

//ADMIN - User Category
export const ADMIN_RESOURCES__USER_CATEGORY_APIS = {
    GET: AUTH_API_URL+'/admin/resources/user-categories/all', //GET, NO TOKEN,
    CREATE: AUTH_API_URL+'/admin/resources/user-categories/create', //POST, NEED TOKEN, (name, parent = null)
    DELETE: AUTH_API_URL+'/admin/resources/user-categories/delete', //POST, NEED TOKEN, (uuid)
};

//ADMIN - User Category Questions...
export const ADMIN_RESOURCES__USER_CATEGORY_QUESTION_APIS = {
    GET: AUTH_API_URL+'/admin/resources/user-categories/questions/all', //GET, NO TOKEN,
    CREATE: AUTH_API_URL+'/admin/resources/user-categories/questions/create', //POST, NEED TOKEN, (question, uuid: category-uuid)
    DELETE: AUTH_API_URL+'/admin/resources/user-categories/questions/delete', //POST, NEED TOKEN, (uuid: question-uuid)
};


//FRONTEND - User Category
export const FRONTEND_RESOURCES__USER_CATEGORY_APIS = {
    GET: AUTH_API_URL+'/frontend/resources/user-categories/all', //GET, NO TOKEN
};
