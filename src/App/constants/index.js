import { keyMirror } from '../modules/helpers';

export const ActionTypes_App = keyMirror({
    LOADED: undefined,

    ALERT__SHOW: undefined,
    ALERT__HIDE: undefined,

    PROGRESS__TASK_ADD: undefined,
    PROGRESS__TASK_DONE: undefined,
    PROGRESS__CLEAR: undefined,

    SAVE_TEXT: undefined,
    DELETE_TEXT: undefined,
    UPDATE_TEXT: undefined,
}, 'APP_');

export const STATUS = {
  IDLE: 'idle',
  RUNNING: 'running',
  READY: 'ready',
  SUCCESS: 'success',
  ERROR: 'error',
};
