import React, {Component} from 'react';

import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/lib/integration/react';
import { HelmetProvider } from 'react-helmet-async';
import { store, persistor } from './store';
import {connect} from 'react-redux';
import {AppLoaded} from './actions';
import Loader from './components/chunks/utils/PageLoader';

import Routes from './Routes';

import '@fortawesome/fontawesome-free/css/all.min.css';
import 'bootstrap-css-only/css/bootstrap.min.css';
import 'mdbreact/dist/css/mdb.css';
import './assets/global.scss';
import './assets/custom.css';
import {ActionTypes_App} from "./constants";

class App extends Component {
    componentDidMount() {
        this.props.dispatch(AppLoaded());
    }

    render() {
        return (
            <PersistGate loading={<Loader size={100} block/>} persistor={persistor}>
                <Loader size={100} block/>
                <HelmetProvider>
                    <Routes/>
                </HelmetProvider>
            </PersistGate>
        );
    }
}

const MyApp = connect(null, null)(App);

class StoreProvider extends Component{
    render() {
        return (
            <Provider store={store}>
                <MyApp/>
            </Provider>
        );
    }
}

export default StoreProvider;
