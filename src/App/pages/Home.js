import React, {useState} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';

import { MDBContainer, MDBRow, MDBCol } from "mdbreact";
import {SaveText, DeleteText, UpdateText} from '../actions';
import {connect} from "react-redux";

const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'column',
        '& > *': {
            margin: theme.spacing(1),
            width: '25ch',
        },
    },
    table: {
        //minWidth: 650,
    },
}));

const Home = (props) => {
    const classes = useStyles();

    //Refs...
    const [txt, setTxt] = useState('');
    const [isEditing, setIsEditing] = useState(false);

    //Handlers...
    const onChangeText = (e) => {
        setTxt(e.target.value);
    };
    const onClickSave = () => {
        const val = txt.trim();
        if(val.length) {
            if(isEditing){
                props.updateText(isEditing.index, val);
            }else {
                props.saveText(val);
            }
        }
        setTxt('');
        setIsEditing(false);
    };
    const onClickDelete = (index) => {
        props.deleteText(index);
    };
    const onClickEdit = (index, txt) => {
        const data = {index, txt};
        setTxt(txt);
        setIsEditing(data);
    };

    return <MDBContainer className="mt-5 text-center">
        <MDBRow>
            <MDBCol size={"6"}>
                <form className={classes.root} noValidate autoComplete="off">
                    <TextField label="Text Here..." value={txt} onChange={onChangeText}/>
                    <Button onClick={onClickSave} color="primary">
                        {isEditing?"Update Now!":"Save Now!"}
                    </Button>
                </form>
            </MDBCol>
            <MDBCol size={"6"}>
                <TableContainer component={Paper}>
                    <Table className={classes.table} aria-label="simple table">
                        <TableHead>
                            <TableRow>
                                <TableCell>#</TableCell>
                                <TableCell align="right">Title</TableCell>
                                <TableCell align="right">Actions</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {props.list.map((item, index) => <TableRow key={index}>
                                    <TableCell component="th" scope="row">{index + 1}</TableCell>
                                    <TableCell align="right">{item}</TableCell>
                                    <TableCell align="right">
                                        <Button onClick={e=>onClickEdit(index, item)} variant="outlined" color="primary">
                                            Edit
                                        </Button>
                                        <Button onClick={e=>onClickDelete(index)} variant="outlined" color="secondary">
                                            Delete
                                        </Button>
                                    </TableCell>
                            </TableRow>)}
                        </TableBody>
                    </Table>
                </TableContainer>
            </MDBCol>
        </MDBRow>
    </MDBContainer>
};

const mapStoreToProps = (store) => ({
    list: store.app.list
});
const mapDispatchToProps = (dispatch) => ({
    saveText: txt => dispatch(SaveText(txt)),
    deleteText: index => dispatch(DeleteText(index)),
    updateText: (index, txt) => dispatch(UpdateText(index, txt)),
});

export default connect(mapStoreToProps, mapDispatchToProps)(Home);
