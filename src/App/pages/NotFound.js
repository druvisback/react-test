import React from 'react';
import { MDBJumbotron, MDBBtn, MDBContainer, MDBRow, MDBCol } from "mdbreact";
import { Link } from 'react-router-dom';
import styled from 'styled-components';

import { Container, Heading } from 'styled-minimal';

const StyledContainer = styled(Container)`
  align-items: center;
  text-align: center;

  h1,
  a {
    color: #fff;
    line-height: 1;
  }

  a {
    text-decoration: underline;
  }
`;

const NotFound = () => (
    <MDBContainer className="mt-5 text-center">
        <MDBRow>
            <MDBCol>
                <MDBJumbotron>
                    <h2 className="h1 display-3">Oops! Invalid Request!</h2>
                    <p className="lead">
                        You try a invalid page, please try again or goto below link for Home Page.
                    </p>
                    <p className="lead">
                        <Link to={'/'}><MDBBtn color="primary">GOTO HOME</MDBBtn></Link>
                    </p>
                </MDBJumbotron>
            </MDBCol>
        </MDBRow>
    </MDBContainer>
);

export default NotFound;
