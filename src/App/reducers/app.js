import { REHYDRATE } from 'redux-persist/lib/constants';
import { handleActions } from '../modules/helpers';

import {ActionTypes_App, STATUS} from '../constants';

export const appState = {
    loaded: false,
    list: []
};

export default {
  app: handleActions(
    {
        [REHYDRATE]: draft => {
            draft.loaded = false;
        },

        //App Reducers...
        [ActionTypes_App.SAVE_TEXT]: (draft, { payload: { txt } }) => {
            draft.list = [txt, ...draft.list];
        },
        [ActionTypes_App.DELETE_TEXT]: (draft, { payload: { index } }) => {
            draft.list = draft.list.filter((item, i) => {
                return i !== index;
            });
        },
        [ActionTypes_App.UPDATE_TEXT]: (draft, { payload: { index, txt } }) => {
            draft.list[index] = txt;
        },

    },
    appState,
  ),
};
