import { applyMiddleware, createStore, compose, combineReducers } from 'redux';
import { persistStore, persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import rootReducer from '../reducers';

import middleware from './middleware';
import { routerMiddleware } from 'react-router-redux';

import browserHistory from '../modules/history';
 
// Apply the middleware to the store
const routerReduxMiddleware = routerMiddleware(browserHistory)



export const env = process.env.NODE_ENV || 'development';

const reducer = persistReducer(
  {
    key: 'rrsb', // key is required
    storage, // storage is now required
    whitelist: ['app'],
  },
  combineReducers({ ...rootReducer }),
);

const composeEnhancer = (env === 'development')?window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose: compose;

  const configStore = (initialState = {}) => {
  const store = createStore(reducer, initialState, composeEnhancer(applyMiddleware(...middleware, routerReduxMiddleware)));

  if (module.hot) {
    module.hot.accept('reducers', () => {
      store.replaceReducer(rootReducer.default);
    });
  }

  return {
    persistor: persistStore(store),
    store,
  };
};

const { store, persistor } = configStore();

global.store = store;

export { store, persistor };
